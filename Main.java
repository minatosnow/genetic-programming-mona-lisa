package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        //Set title
        primaryStage.setTitle("Evolving Semi-Transparent Polygons into Work of Art");
        Scene scene = new Scene(root);

        //Adding scene to stage
        primaryStage.setScene(scene);
        //Display contents of the stage
        primaryStage.show();

    }



    public static void main(String[] args) {
        launch(args);
    }

}
