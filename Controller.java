package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.File;


public class Controller {

    @FXML
    ImageView imageView;
    Image originImage;

    public Label label;
    private Window stage;
    final Label fileLabel = new Label();
    private File file;



    public void initialize() {
        // TODO
    }

    //Open Image Action to change scene
    public void openImageAction(ActionEvent event) throws Exception {
        FileChooser fileChooser = new FileChooser();
        //Filter for images
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Picture Files", "*.*"),
                new FileChooser.ExtensionFilter("PNG (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPEG (*.jpg;*.jpeg;*.jpe;)", "*.jpg;*.jpeg;*.jpe")

        );

        fileChooser.setTitle("Open Picture Files");
        file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            fileLabel.setText(file.getPath());
            originImage = new Image(file.toURI().toString());
            imageView = new ImageView(originImage);

            //Change scene after importing image
            AnchorPane mainParent = FXMLLoader.load(getClass().getResource("main.fxml"));
            Scene mainScene = new Scene(mainParent);
            mainParent.getChildren().add(imageView);
            //Set size for imageView1
            imageView.setFitHeight(600);
            imageView.setFitWidth(450);
            imageView.setSmooth(true);
            imageView.setCache(true);

            //Adding scene to stage
            Stage scene = (Stage) ((Node) event.getSource()).getScene().getWindow();
            //Display contents of the stage
            scene.setScene(mainScene);
            scene.show();
        }
    }
}